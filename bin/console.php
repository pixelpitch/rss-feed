<?php

use App\Application\Command\ImportFeedCommand;
use Symfony\Component\Console\Application;

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../container-build-up.php';

/** @var Container $container */
$container = require_once __DIR__.'/../bootstrap.php';

$application = new Application(
    $container->get('app')['name'],
    $container->get('app')['version']
);

$application->addCommands([
    $container->get(ImportFeedCommand::class)
]);

$application->run();
