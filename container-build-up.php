<?php

use DI\ContainerBuilder;
use function DI\env;

// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

$dev = env('DEV', false);

if (!$dev) { // Should be set to true in production
    $containerBuilder->enableCompilation(__DIR__.'/../var/cache');
}

// Set up settings
$settings = require __DIR__.'/app/settings.php';
$settings($containerBuilder);

// Set up dependencies
$dependencies = require __DIR__.'/app/dependencies.php';
$dependencies($containerBuilder);

// Set up repositories
$repositories = require __DIR__.'/app/repositories.php';
$repositories($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();
