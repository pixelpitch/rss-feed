<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        'twig.engine' => function (ContainerInterface $c) {
            $loader = new FilesystemLoader($c->get('twig')['templates']['generic']);

            $loader->addPath(
                $c->get('twig')['templates']['generic']
                .'/'
                .$c->get('twig')['templates']['layout'], 'layout'
            );
            $loader->addPath(
                $c->get('twig')['templates']['generic']
                .'/'
                .$c->get('twig')['templates']['user'], 'user'
            );
            $loader->addPath(
                $c->get('twig')['templates']['generic']
                .'/'
                .$c->get('twig')['templates']['admin'], 'admin'
            );

            $twig = new Environment($loader, [
                'cache' => $c->get('twig')['cache'],
            ]);

            return $twig;
        },
    ]);
};
