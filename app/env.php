<?php

use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();

$dotenv->load($appRoot.'/.env', $appRoot.'/.env.dev');
