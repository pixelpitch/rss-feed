<?php

use App\Application\Command\ImportFeedCommand;
use App\Repository\FeedRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

$container->set(ImportFeedCommand::class, function (ContainerInterface $c) {
    $importFeedCommand = new ImportFeedCommand(
        $c->get(FeedRepository::class),
        $c->get('data')['feed'],
        $c->get(EntityManagerInterface::class)
    );

    return $importFeedCommand;
});

return $container;
