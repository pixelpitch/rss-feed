<?php

use App\Application\Entity\Feed;
use App\Application\Entity\Feedentry;
use App\Repository\FeedentryRepository;
use App\Repository\FeedRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

$container->set(FeedRepository::class, function (ContainerInterface $c) {
    $entityManager = $c->get(EntityManagerInterface::class);
    return $entityManager->getRepository(Feed::class);
});
$container->set(FeedentryRepository::class, function (ContainerInterface $c) {
    $entityManager = $c->get(EntityManagerInterface::class);
    return $entityManager->getRepository(Feedentry::class);
});

return $container;
