<?php

declare(strict_types=1);

use App\Application\Actions\Feed\ListFeedsAction;
use App\Application\Actions\Feed\ViewFeedAction;
use App\Application\Actions\FeedEntry\ListFeedEntriesAction;
use App\Application\Actions\FeedEntry\ViewFeedEntryAction;
use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');

        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    $app->group('/feeds', function (Group $group) {
        $group->get('', ListFeedsAction::class);
        $group->get('/{id}', ViewFeedAction::class);
    });

    $app->group('/feed-entries', function (Group $group) {
        $group->get('', ListFeedEntriesAction::class);
        $group->get('/{id}', ViewFeedEntryAction::class);
    });
};
