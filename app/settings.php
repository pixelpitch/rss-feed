<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;
use function DI\env;

$appRoot = __DIR__.'/..';

require __DIR__.'/env.php';

return function (ContainerBuilder $containerBuilder) use ($appRoot) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__.'/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
        ],
        'app' => [
            'name' => 'RSS Feed reader',
            'version' => 'v1.0.4.1',
        ],
        'doctrine' => [
            // if true, metadata caching is forcefully disabled
            'dev_mode' => env('DEV', false),

            // path where the compiled metadata info will be cached
            // make sure the path exists and it is writable
            'cache_dir' => $appRoot.'/var/doctrine',

            // you should add any other path containing annotated entity classes
            'metadata_dirs' => [$appRoot.'/mapping'],

            'connection' => [
                'driver' => 'pdo_mysql',
                'host' => env('DB_HOST'),
                'port' => env('DB_PORT'),
                'dbname' => env('DB_NAME'),
                'user' => env('DB_USER'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8mb4',
            ],
            'proxies' => $appRoot.'/var/proxies',
            'proxy.namespaces' => 'App\Proxy',
            'entity.namespaces' => 'App\Application\Entity',
        ],
        'twig' => [
            'templates' => [
                'generic' => __DIR__.'/../views',
                'layout' => 'layout',
                'user' => 'user',
                'admin' => 'admin',
            ],
            'cache' => __DIR__.'/../var/twig',
        ],
        'data' => [
            'feed' => __DIR__.'/../data/*.xml',
        ],
    ]);
};
