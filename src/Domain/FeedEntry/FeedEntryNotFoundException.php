<?php

namespace App\Domain\FeedEntry;

use App\Domain\DomainException\DomainRecordNotFoundException;

/**
 * Feed not found exception.
 */
class FeedEntryNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The feed entry you requested does not exist.';
}
