<?php

namespace App\Domain\Feed;

use App\Domain\DomainException\DomainRecordNotFoundException;

/**
 * Feed not found exception.
 */
class FeedNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The feed you requested does not exist.';
}
