<?php

namespace App\Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Feedentry.
 *
 * @ORM\Table(name="feedEntry", indexes={@ORM\Index(name="field", columns={"field"})})
 * @ORM\Entity(repositoryClass="App\Repository\FeedentryRepository")
 */
class Feedentry implements JsonSerializable
{
    /**
     * @var uuid
     *
     * @ORM\Column(name="id", type="uuid", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="updated", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="authorName", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $authorname;

    /**
     * @var int|null
     *
     * @ORM\Column(name="authorUri", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $authoruri;

    /**
     * @var int
     *
     * @ORM\Column(name="link", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $link;

    /**
     * @var int
     *
     * @ORM\Column(name="title", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="summary", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $summary;

    /**
     * @var \App\Application\Entity\Feed
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Entity\Feed", inversedBy="entries")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field", referencedColumnName="id", nullable=true)
     * })
     */
    private $field;

    /**
     * JSON dataset.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'updated' => $this->updated,
            'authorName' => $this->authorname,
            'authorUri' => $this->authoruri,
            'link' => $this->link,
            'title' => $this->title,
            'summary' => $this->summary,
        ];
    }

    /**
     * Get id.
     *
     * @return uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set updated.
     *
     * @param int $updated
     *
     * @return Feedentry
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set authorname.
     *
     * @param int $authorname
     *
     * @return Feedentry
     */
    public function setAuthorname($authorname)
    {
        $this->authorname = $authorname;

        return $this;
    }

    /**
     * Get authorname.
     *
     * @return int
     */
    public function getAuthorname()
    {
        return $this->authorname;
    }

    /**
     * Set authoruri.
     *
     * @param int|null $authoruri
     *
     * @return Feedentry
     */
    public function setAuthoruri($authoruri = null)
    {
        $this->authoruri = $authoruri;

        return $this;
    }

    /**
     * Get authoruri.
     *
     * @return int|null
     */
    public function getAuthoruri()
    {
        return $this->authoruri;
    }

    /**
     * Set link.
     *
     * @param int $link
     *
     * @return Feedentry
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return int
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set title.
     *
     * @param int $title
     *
     * @return Feedentry
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set summary.
     *
     * @param int $summary
     *
     * @return Feedentry
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary.
     *
     * @return int
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set field.
     *
     * @param \App\Application\Entity\Feed|null $field
     *
     * @return Feedentry
     */
    public function setField(\App\Application\Entity\Feed $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field.
     *
     * @return \App\Application\Entity\Feed|null
     */
    public function getField()
    {
        return $this->field;
    }
}
