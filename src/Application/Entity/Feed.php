<?php

namespace App\Application\Entity;

use JsonSerializable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Feed.
 *
 * @ORM\Table(name="feed")
 * @ORM\Entity(repositoryClass="App\Repository\FeedRepository")
 */
class Feed implements JsonSerializable
{
    /**
     * @var uuid
     *
     * @ORM\Column(name="id", type="uuid", precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=512, precision=0, scale=0, nullable=false, unique=false)
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="altLink", type="string", length=512, precision=0, scale=0, nullable=true, unique=false)
     */
    private $altlink;

    /**
     * @var int
     *
     * @ORM\Column(name="authorName", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $authorname;

    /**
     * @var int
     *
     * @ORM\Column(name="authorEmail", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $authoremail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="authorLink", type="string", length=256, precision=0, scale=0, nullable=true, unique=false)
     */
    private $authorlink;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $logo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="rights", type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    private $rights;

    /**
     * Feed entries.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Feedentry", mappedBy="field", cascade={"persist", "remove"})
     */
    private $entries;

    /**
     * JSON dataset.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'link' => $this->link,
            'altLink' => $this->altlink,
            'authorName' => $this->authorname,
            'authorEmail' => $this->authoremail,
            'authorLink' => $this->authorlink,
            'icon' => $this->icon,
            'subtitle' => $this->subtitle,
            'logo' => $this->logo,
            'updated' => $this->updated,
        ];
    }

    /**
     * Get id.
     *
     * @return uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Feed
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Feed
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set altlink.
     *
     * @param string|null $altlink
     *
     * @return Feed
     */
    public function setAltlink($altlink = null)
    {
        $this->altlink = $altlink;

        return $this;
    }

    /**
     * Get altlink.
     *
     * @return string|null
     */
    public function getAltlink()
    {
        return $this->altlink;
    }

    /**
     * Set authorname.
     *
     * @param int $authorname
     *
     * @return Feed
     */
    public function setAuthorname($authorname)
    {
        $this->authorname = $authorname;

        return $this;
    }

    /**
     * Get authorname.
     *
     * @return int
     */
    public function getAuthorname()
    {
        return $this->authorname;
    }

    /**
     * Set authoremail.
     *
     * @param int $authoremail
     *
     * @return Feed
     */
    public function setAuthoremail($authoremail)
    {
        $this->authoremail = $authoremail;

        return $this;
    }

    /**
     * Get authoremail.
     *
     * @return int
     */
    public function getAuthoremail()
    {
        return $this->authoremail;
    }

    /**
     * Set authorlink.
     *
     * @param string|null $authorlink
     *
     * @return Feed
     */
    public function setAuthorlink($authorlink = null)
    {
        $this->authorlink = $authorlink;

        return $this;
    }

    /**
     * Get authorlink.
     *
     * @return string|null
     */
    public function getAuthorlink()
    {
        return $this->authorlink;
    }

    /**
     * Set icon.
     *
     * @param string $icon
     *
     * @return Feed
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set subtitle.
     *
     * @param string $subtitle
     *
     * @return Feed
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle.
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set logo.
     *
     * @param string $logo
     *
     * @return Feed
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Feed
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set rights.
     *
     * @param string $rights
     *
     * @return Feed
     */
    public function setRights($rights)
    {
        $this->rights = $rights;

        return $this;
    }

    /**
     * Get rights.
     *
     * @return string
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * Get feed entries.
     *
     * @return ArrayCollection
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Set feed entries.
     *
     * @param ArrayCollection $entries feed entries
     *
     * @return self
     */
    public function setEntries(ArrayCollection $entries)
    {
        $this->entries = $entries;

        return $this;
    }
}
