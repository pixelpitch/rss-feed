<?php

namespace App\Application\Actions\Feed;

use App\Application\Actions\Action;
use App\Repository\FeedRepository;

/**
 * General feed action.
 */
abstract class FeedAction extends Action
{
    /**
     * Feed repository.
     *
     * @var FeedRepository
     */
    protected $feedRepository;

    /**
     * Constructor.
     *
     * @param FeedRepository $feedRepository
     */
    public function __construct(FeedRepository $feedRepository)
    {
        $this->feedRepository = $feedRepository;
    }
}
