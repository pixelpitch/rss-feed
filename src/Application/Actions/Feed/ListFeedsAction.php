<?php

namespace App\Application\Actions\Feed;

/**
 * Listing feeds.
 */
class ListFeedsAction extends FeedAction
{
    /**
     * Action implementation.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function action(): \Psr\Http\Message\ResponseInterface
    {
        $feedRepository = $this->feedRepository;

        $feeds = $feedRepository->findAll();

        return $this->respondWithData($feeds);
    }
}
