<?php

namespace App\Application\Actions\Feed;

use App\Domain\Feed\FeedNotFoundException;

class ViewFeedAction extends FeedAction
{
    protected function action(): \Psr\Http\Message\ResponseInterface
    {
        $feedRepository = $this->feedRepository;

        $id = $this->resolveArg('id');
        $feed = $feedRepository->find($id);

        if (!$feed) {
            throw new FeedNotFoundException();
        }

        return $this->respondWithData($feed);
    }
}
