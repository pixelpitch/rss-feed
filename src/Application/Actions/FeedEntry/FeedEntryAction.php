<?php

namespace App\Application\Actions\FeedEntry;

use App\Application\Actions\Action;
use App\Repository\FeedentryRepository;

/**
 * General feed entry action.
 */
abstract class FeedEntryAction extends Action
{
    /**
     * Feed entry repository.
     *
     * @var FeedentryRepository
     */
    protected $feedEntryRepository;

    /**
     * Constructor.
     *
     * @param FeedentryRepository $feedEntryRepository
     */
    public function __construct(FeedentryRepository $feedEntryRepository)
    {
        $this->feedEntryRepository = $feedEntryRepository;
    }
}
