<?php

namespace App\Application\Actions\FeedEntry;

/**
 * Listing feed entries.
 */
class ListFeedEntriesAction extends FeedEntryAction
{
    /**
     * Action implementationn.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function action(): \Psr\Http\Message\ResponseInterface
    {
        $feedEntryRepository = $this->feedEntryRepository;

        $feedEntries = $feedEntryRepository->findAll();

        return $this->respondWithData($feedEntries);
    }
}
