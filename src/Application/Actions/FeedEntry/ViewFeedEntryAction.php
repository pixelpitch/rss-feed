<?php

namespace App\Application\Actions\FeedEntry;

use App\Domain\FeedEntry\FeedEntryNotFoundException;

/**
 * View individual feed entry.
 */
class ViewFeedEntryAction extends FeedEntryAction
{
    /**
     * Action implementation.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function action(): \Psr\Http\Message\ResponseInterface
    {
        $feedEntryRepository = $this->feedEntryRepository;

        $id = $this->resolveArg('id');
        $feedEntry = $feedEntryRepository->find($id);

        if (!$feedEntry) {
            throw new FeedEntryNotFoundException();
        }

        return $this->respondWithData($feedEntry);
    }
}
