<?php

namespace App\Application\Command;

use App\Application\Entity\Feed;
use App\Repository\FeedRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Import feed.
 */
class ImportFeedCommand extends Command
{
    protected static $defaultName = 'app:import-feed';

    /**
     * Feed repository.
     *
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * Feed path.
     *
     * @var string
     */
    private $feedPath;

    /**
     * Entity manager.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Constructor.
     *
     * @param FeedRepository] $feedRepository
     * @param string          $feedPath
     */
    public function __construct(FeedRepository $feedRepository, string $feedPath, EntityManagerInterface $entityManagger)
    {
        parent::__construct();

        $this->feedRepository = $feedRepository;
        $this->feedPath = $feedPath;
        $this->entityManager = $entityManagger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Imports feed to the database.')
            ->setHelp('Import a feed to the database instead of manually adding it from xml file.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $feedRepository = $this->feedRepository;
        $feedPath = $this->feedPath;
        $entityManager = $this->entityManager;

        $entries = glob($feedPath);

        $progressBar = new ProgressBar($output);

        foreach ($progressBar->iterate($entries) as $entry) {
            $xml = simplexml_load_file($entry);
            $currentItem = $feedRepository->findBy(
                [
                    'title' => $xml->title,
                    'link' => $xml->link[0]->attributes()['href'],
                ]
            );
            if ($currentItem) {
                continue;
            }

            $feed = new Feed();
            $feed->setAuthorname($xml->author->name);
            $feed->setAuthoremail($xml->author->email);
            $feed->setAuthorlink($xml->author->uri);
            $feed->setTitle($xml->title);
            $feed->setLink($xml->link[0]->attributes()['href']);
            $feed->setAltlink($xml->link[1]->attributes()['href']);
            $feed->setRights($xml->rights);
            $feed->setIcon($xml->icon);
            $feed->setSubtitle($xml->subtitle);
            $feed->setLogo($xml->logo);
            $updatedAt = str_replace('T', ' ', $xml->updated);
            $feed->setUpdated(new \DateTime($updatedAt));

            $entityManager->persist($feed);
            $entityManager->flush();

            $progressBar->advance();
        }

        $info = $output->section();
        $info->writeln(PHP_EOL.'<info>Data imported</info>');
    }
}
