<?php

use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\Doctrine\UuidType;

require_once __DIR__.'/vendor/autoload.php';

$container->set(EntityManagerInterface::class, function (ContainerInterface $container): EntityManager {
    \Doctrine\DBAL\Types\Type::addType('uuid', UuidType::class);

    $config = Setup::createAnnotationMetadataConfiguration(
        $container->get('doctrine')['metadata_dirs'],
        $container->get('doctrine')['dev_mode'],
        $container->get('doctrine')['proxies'],
        new FilesystemCache(
            $container->get('doctrine')['cache_dir']
        ),
        false
    );

    $config->setProxyNamespace($container->get('doctrine')['proxy.namespaces']);
    $config->setEntityNamespaces([$container->get('doctrine')['entity.namespaces']]);

    return EntityManager::create(
        $container->get('doctrine')['connection'],
        $config
    );
});

/**
 * @var ContainerInterface
 */
$container = include __DIR__.'/app/doctrine-repositories.php';

/**
 * @var ContainerInterface
 */
$container = include __DIR__.'/app/commands.php';

return $container;
