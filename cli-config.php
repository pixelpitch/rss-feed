<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use DI\Container;
use Doctrine\ORM\EntityManagerInterface;

require __DIR__.'/container-build-up.php';

/** @var Container $container */
$container = require_once __DIR__.'/bootstrap.php';

ConsoleRunner::run(
    ConsoleRunner::createHelperSet($container->get(EntityManagerInterface::class))
);
