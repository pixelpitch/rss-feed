<?php

namespace Tests\Application\Actions\Feed;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Entity\Feedentry;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\FeedEntry\FeedEntryNotFoundException;
use App\Repository\FeedentryRepository;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;

class ViewFeedEntryActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $feedEntry = new Feedentry();
        $feedEntry
            ->setLink('pbgroupeu.wordpress.com/portfolio')
            ->setTitle('ECB downgrades its distro upgrade policies')
            ->setSummary('ECB alongside FED, following their example, downgraded init policy to level 3'
                .' due to environmental Wifi restrictions')
            ->setUpdated(new \Datetime())
        ;

        $feedEntryRepositoryProphecy = $this->prophesize(FeedentryRepository::class);
        $feedEntryRepositoryProphecy
            ->find('f2118e35-135f-4187-b20a-10f2855e6e8a')
            ->willReturn($feedEntry);

        $container->set(FeedentryRepository::class, $feedEntryRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/feed-entries/f2118e35-135f-4187-b20a-10f2855e6e8a');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, $feedEntry);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionThrowsFeedNotFoundException()
    {
        $app = $this->getAppInstance();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $app->add($errorMiddleware);

        /** @var Container $container */
        $container = $app->getContainer();

        $feedEntryRepositoryProphecy = $this->prophesize(FeedentryRepository::class);
        $feedEntryRepositoryProphecy
            ->find('f2118e35-135f-4187-b20a-10f2855e6e8a')
            ->willThrow(new FeedEntryNotFoundException())
            ->shouldBeCalledOnce()
        ;

        $container->set(FeedentryRepository::class, $feedEntryRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/feed-entries/f2118e35-135f-4187-b20a-10f2855e6e8a');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::RESOURCE_NOT_FOUND, 'The feed entry you requested does not exist.');
        $expectedPayload = new ActionPayload(404, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
