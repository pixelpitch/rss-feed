<?php

namespace Tests\Application\Actions\Feed;

use App\Application\Actions\ActionPayload;
use App\Application\Entity\Feedentry;
use App\Repository\FeedentryRepository;
use Tests\TestCase;

class ListFeedsActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $feedEntry = new Feedentry();
        $feedEntry
            ->setLink('pbgroupeu.wordpress.com/portfolio')
            ->setTitle('ECB downgrades its distro upgrade policies')
            ->setSummary('ECB alongside FED, following their example, downgraded init policy to level 3'
                .' due to environmental Wifi restrictions')
            ->setUpdated(new \Datetime())
            ;

        $feedEntryRepositoryProphecy = $this->prophesize(FeedentryRepository::class);
        $feedEntryRepositoryProphecy
            ->findAll()
            ->willReturn([$feedEntry])
            ->shouldBeCalledOnce();

        $container->set(FeedentryRepository::class, $feedEntryRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/feed-entries');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, [$feedEntry]);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
