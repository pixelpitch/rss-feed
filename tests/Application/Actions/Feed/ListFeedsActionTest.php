<?php

namespace Tests\Application\Actions\Feed;

use App\Application\Actions\ActionPayload;
use App\Application\Entity\Feed;
use App\Repository\FeedRepository;
use Tests\TestCase;

class ListFeedsActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $feed = new Feed();
        $feed
            ->setAuthorname('F. Keynes')
            ->setAuthoremail('keynes@fed.usa.gov')
            ->setIcon('pbgroup.com')
            ->setLogo('pbgroup.com-square.png')
            ->setTitle('Macroeconomics decadence')
            ->setSubtitle('Media giants proliferation')
            ->setUpdated(new \DateTime())
            ->setLink('pbgroupeu.wordpress.com')
            ;

        $feedRepositoryProphecy = $this->prophesize(FeedRepository::class);
        $feedRepositoryProphecy
            ->findAll()
            ->willReturn([$feed])
            ->shouldBeCalledOnce();

        $container->set(FeedRepository::class, $feedRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/feeds');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, [$feed]);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
