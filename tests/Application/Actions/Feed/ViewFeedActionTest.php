<?php

namespace Tests\Application\Actions\Feed;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Entity\Feed;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\Feed\FeedNotFoundException;
use App\Repository\FeedRepository;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;

class ViewFeedActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $feed = new Feed();
        $feed
            ->setAuthorname('F. Keynes')
            ->setAuthoremail('keynes@fed.usa.gov')
            ->setIcon('pbgroup.com')
            ->setLogo('pbgroup.com-square.png')
            ->setTitle('Macroeconomics decadence')
            ->setSubtitle('Media giants proliferation')
            ->setUpdated(new \DateTime())
            ->setLink('pbgroupeu.wordpress.com')
            ;

        $feedRepositoryProphecy = $this->prophesize(FeedRepository::class);
        $feedRepositoryProphecy
            ->find('c3cd8b23-42d4-48dd-a209-38570a91c0c1')
            ->willReturn($feed)
            ->shouldBeCalledOnce();

        $container->set(FeedRepository::class, $feedRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/feeds/c3cd8b23-42d4-48dd-a209-38570a91c0c1');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, $feed);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionThrowsFeedNotFoundException()
    {
        $app = $this->getAppInstance();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $app->add($errorMiddleware);

        /** @var Container $container */
        $container = $app->getContainer();

        $feedRepositoryProphecy = $this->prophesize(FeedRepository::class);
        $feedRepositoryProphecy
            ->find('c3cd8b23-42d4-48dd-a209-38570a91c0c1')
            ->willThrow(new FeedNotFoundException())
            ->shouldBeCalledOnce();

        $container->set(FeedRepository::class, $feedRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/feeds/c3cd8b23-42d4-48dd-a209-38570a91c0c1');
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::RESOURCE_NOT_FOUND, 'The feed you requested does not exist.');
        $expectedPayload = new ActionPayload(404, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
