<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * Feedentry.
 *
 * @ORM\Table(name="feedEntry", indexes={@ORM\Index(name="field", columns={"field"})})
 * @ORM\Entity(repositoryClass="App\Repository\FeedentryRepository")
 */
class Feedentry
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="updated", type="integer", nullable=false)
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="authorName", type="integer", nullable=false)
     */
    private $authorname;

    /**
     * @var int|null
     *
     * @ORM\Column(name="authorUri", type="integer", nullable=true)
     */
    private $authoruri;

    /**
     * @var int
     *
     * @ORM\Column(name="link", type="integer", nullable=false)
     */
    private $link;

    /**
     * @var int
     *
     * @ORM\Column(name="title", type="integer", nullable=false)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="summary", type="integer", nullable=false)
     */
    private $summary;

    /**
     * @var \App\Application\Entity\Feed
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Entity\Feed", inversedBy="entries")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field", referencedColumnName="id")
     * })
     */
    private $field;
}
