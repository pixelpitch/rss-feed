<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * Feed.
 *
 * @ORM\Table(name="feed")
 * @ORM\Entity(repositoryClass="App\Repository\FeedRepository")
 */
class Feed
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=512, nullable=false)
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="altLink", type="string", length=512, nullable=true)
     */
    private $altlink;

    /**
     * @var int
     *
     * @ORM\Column(name="authorName", type="string", length=256, nullable=false)
     */
    private $authorname;

    /**
     * @var int
     *
     * @ORM\Column(name="authorEmail", type="string", length=256, nullable=false)
     */
    private $authoremail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="authorLink", type="string", length=256, nullable=true)
     */
    private $authorlink;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=256, nullable=false)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=256, nullable=false)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=256, nullable=false)
     */
    private $logo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="rights", type="string", length=256, nullable=false)
     */
    private $rights;

    /**
     * Feed entries.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Feedentry", mappedBy="field", cascade={"persist", "remove"})
     */
    private $entries;
}
